#ifndef _LIB_LIBHMD_THREAD_H_
#define _LIB_LIBHMD_THREAD_H_

#include <pthread.h>
#include <unistd.h>
#include <stdint.h>

/**
 * @file libhmd_thread.h
 * 
 * @brief Health Monitor process monitor registration.
 *
 * The caller should have prepared a callback function.
 * This callback function should quickly start the monitored process and
 * perform several steps:
 * @code
 * // 1. Retrieve parameters
 * // 2. Lock the mutex. This does not need to be unlocked.
 * // 3. Start subprocess
 * // 4. Set PID
 * // 5. Register cleanup code with pthread_cleanup_push(3) if needed
 * // 6. Enter main loop
 * // Example of step 1.
 * hmd_thread_arg_fs* x = arg;
 * pthread_mutex_lock(&x->thread_lock);
 * void* cb_arg = x->cb_arg;
 * x->pid = p; // PID of started process
 * @endcode
 *
 * Threads can be stopped and started as desired with
 * libhmd_enable and libhmd_disable.
 * 
 */

struct hmd_thread_arg_fs {
    void* cb_arg;
    pthread_mutex_t thread_lock;
    pid_t pid;
};

typedef struct hmd_thread_arg_fs hmd_thread_arg_fs;

/**
 * @brief Called once in a program to initialize data structres.
 *
 * This function can be considered atomic. If it fails, all progress will be
 * torn down and the function can then be called again. Otherwise, this
 * function should not be called twice unless it is closed first.
 *
 * This will register (but not enable) a thread for powerboard and ADCS.
 *
 * @return EXIT_SUCCESS. EXIT_FAILURE if any error occurs.
 *
 * @see hmd_thread_close
 */
int hmd_thread_init(void);
/**
 * @brief Disables and deregisters all threads. Tears down all data structures.
 *
 * This function assumes nothing fails. If anything does, the program should
 * still be in a state that can be init-ed again.
 *
 * @return EXIT_SUCCESS.
 */
int hmd_thread_close(void);
/**
 * @brief Enable the powerboard and ADCS threads.
 *
 * This function will always try to enable both threads for safety.
 * If this function fails, it is safe to continually call it until it 
 * returns EXIT_SUCCESS (though this may not be a good idea).
 *
 * @param powerboard If non-zero, enable default powerboard thread
 * @param adcs If non-zero, enable default adcs
 *
 * @return EXIT_SUCCESS. EXIT_FAILURE if either thread fails to enable.
 */
int hmd_thread_enable_defaults(int powerboard, int adcs);
/**
 * @brief Register (but do not enable) a new thread.
 *
 * @param thread_name 4-character thread name, NULL terminated.
 * @param callback Callback function passed to pthread_create. Return value ignored
 * @param cb_arg Argument passed to the @callback function.
 *
 * @return EXIT_SUCCESS or EXIT_FAILURE if anything goes wrong.
 */
int hmd_thread_register(const char thread_name[5], void* (*callback)(void*), void* cb_arg);
/**
 * @brief Completely removes thread from internal data. Disables it first.
 *
 * @param thread_name The name of the thread to deregister.
 *
 * @return EXIT_SUCCESS if deregistered. EXIT_FAILURE else.
 */
int hmd_thread_deregister(const char thread_name[5]);
/**
 * @brief Starts the registered thread.
 *
 * If the thread is already enabled (pid > 0), returns successfully early.
 *
 * Can be called until it returns successfully.
 *
 * @param thread_name The thread to enable.
 *
 * @return EXIT_SUCCESS. EXIT_FAILURE if anything goes wrong.
 */
int hmd_thread_enable(const char thread_name[5]);
/**
 * @brief Disables the thread by canceling it.
 *
 * Returns successfully early if already disabled.
 * If return fails, program is in an unknown state.
 *
 * @param thread_name The thread to disable.
 *
 * @return EXIT_SUCCESS. EXIT_FAILURE if anything goes wrong.
 */
int hmd_thread_disable(const char thread_name[5]);
/**
 * @brief Returns the pid for the process the thread is monitoring.
 *
 * @param thread_name The thread to get the PID of the process for.
 * @param pid Location to store the pid.
 *
 * @return EXIT_SUCCESS. EXIT_FAILURE if not found or the process was not running.
 */
int hmd_thread_get_pid(const char thread_name[5], pid_t* pid);
/**
 * @brief Checks if the thread is still alive
 *
 * If the thread is not alive, you probably want to re-enable it.
 *
 * @param thread_name The thread to check
 * @param errno_out errno as returned by pthread_mutex_trylock, or NULL to ignore
 *
 * @return EXIT_SUCCESS (implies errno == EBUSY). EXIT_FAILURE else.
 * Depending on the errno, decisions can be made. Likely re-enabling
 * or re-registering will help.
 */
int hmd_thread_check(const char thread_name[5], int* errno_out);
/**
 * @brief Check if each thread is alive
 *
 * Equivalent to calling hmd_thread_check on each enabled thread.
 *
 * @return EXIT_SUCCESS. EXIT_FAILURE if any dead
 */
int hmd_thread_check_all(void);

#endif
