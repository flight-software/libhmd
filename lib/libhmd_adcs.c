#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdint.h>

#include "libdebug.h"
#include "libdaemon.h"
#include "libadcs.h"
#include "librpc.h"
#include "libhmd_thread.h"
#include "libhmd_adcs.h"
#include "libhmd.h"

#define RPC_WAIT (1*1000*1000) // 1 second

static void convert_health_to_beacon(ADCS_HEALTH_PACKET* health, hmd_adcs_default_beacon_fs* beacon);

static hmd_adcs_default_beacon_fs current_beacon;

void* hmd_adcs_thread(void* arg)
{
    P_DEBUG_STR("Started hmd_acd_thread");

    hmd_thread_arg_fs* args = arg;

    pthread_mutex_lock(&args->thread_lock);

    if(is_daemon_alive(ACD_BIN)) {
        P_ERR_STR("ADCS was dead, let's restart it");
        startup_daemon(ACD_BIN);
        sleep(5);
    }

    while(rpc_recv_buf(ACD_RPC_SOCKET, RPC_OP_GET_PID, &args->pid, sizeof(args->pid), RPC_WAIT * 5)) {
        P_ERR_STR("Failed to ask ADCS for its PID");
        if(is_daemon_alive(ACD_BIN)) {
            P_ERR_STR("ADCS was dead, let's restart it");
            args->pid = -1;
            if(startup_daemon(ACD_BIN)) {
                P_ERR_STR("Failed to start ADCS daemon. Keep trying");
            }
        }
        sleep(5);
    }
    
    P_INFO("Got ADCS PID: %d", (int)args->pid);
    
    hmd_write_pid_to_file(HMD_PID_FILE(ACD_BIN), args->pid);

    ADCS_HEALTH_PACKET adcs_health;

    while(1) {
        memset(&adcs_health, 0, sizeof(adcs_health));

        while(is_daemon_alive(ACD_BIN)) {
            P_ERR_STR("ADCS is dead, let's restart it");
            args->pid = -1;
            if(startup_daemon(ACD_BIN)) {
                P_ERR_STR("Failed to start ADCS daemon. Keep trying");
                sleep(5);
            } else {
                sleep(5);
                if(rpc_recv_buf(ACD_RPC_SOCKET, RPC_OP_GET_PID, &args->pid, sizeof(args->pid), RPC_WAIT * 5)) {
                    P_ERR_STR("Failed to get ADCS pid, continuing anyways");
                }
            }
        }

        if(rpc_recv_buf(ACD_RPC_SOCKET, ADCS_OP_GET_HEALTH, &adcs_health, sizeof(adcs_health), RPC_WAIT * 5)) {
            P_ERR_STR("Failed to get ADCS health");
        } else {
            convert_health_to_beacon(&adcs_health, &current_beacon);
        }

        sleep(10);
    }
    return NULL;
}

int hmd_adcs_thread_get_beacon(hmd_adcs_default_beacon_fs* copy)
{
    if(!copy) {
        P_ERR_STR("Got NULL copy");
        return EXIT_FAILURE;
    }
    memcpy(copy, &current_beacon, sizeof(current_beacon));
    return EXIT_SUCCESS;
}

static void convert_health_to_beacon(ADCS_HEALTH_PACKET* health, hmd_adcs_default_beacon_fs* beacon)
{
    memcpy(beacon->quaternions, health->attitudeEstimate, sizeof(current_beacon.quaternions));
    memcpy(beacon->rates, health->rateEstimate, sizeof(current_beacon.rates));
    beacon->illumination_state = 0; // TODO: ???
    beacon->algorithm = health->cur_det;
}

