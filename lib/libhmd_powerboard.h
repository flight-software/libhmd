#ifndef _LIB_LIBHMD_POWERBOARD_H_
#define _LIB_LIBHMD_POWERBOARD_H_

#include "powerboard.h"

struct hmd_pbd_default_beacon_fs {
    double battery_voltage[PBD_LEN_PACK];
    float battery_temp[PBD_LEN_PACK];
};

typedef struct hmd_pbd_default_beacon_fs hmd_pbd_default_beacon_fs;

void* hmd_pbd_thread(void* arg);
int hmd_pbd_thread_get_beacon(hmd_pbd_default_beacon_fs* copy);

#endif
