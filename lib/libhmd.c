#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>

#include "libdebug.h"
#include "librpc.h"
#include "powerboard.h"
#include "libhmd.h"

int fire_thermal_knives(int max_tries, useconds_t wait_us)
{
    for(int i = 0; i < max_tries; i++) {
        P_INFO("Attempting to enable thermal knives: %d/%d", i+1, max_tries);
        if(rpc_send_uint32(PBD_RPC_SOCKET, PBD_OP_HS_EN, HOTSWAP_CH7_PYRO, PBD_OP_WAIT)) {
            P_ERR_STR("Unable to fire thermal knives");
            usleep(wait_us);
            continue;
        }
        return EXIT_SUCCESS;
    }
    return EXIT_FAILURE;
}

int hmd_write_pid_to_file(const char* pid_file, pid_t pid)
{
    if(!pid_file) {
        P_ERR_STR("Got NULL pid_file");
        return EXIT_FAILURE;
    }
    FILE* fp = fopen(pid_file, "w");
    if(!fp) {
        P_ERR("fopen returned NULL for %s, errno: %d (%s)", pid_file, errno, strerror(errno));
        return EXIT_FAILURE;
    }
    if(fwrite(&pid, sizeof(pid), 1, fp) != 1) {
        P_ERR("Failed to write pid to %s, errno: %d (%s)", pid_file, errno, strerror(errno));
        fclose(fp);
        return EXIT_FAILURE;
    }
    if(fclose(fp)) {
        P_ERR("Failed to fclose %s, errno: %d (%s)", pid_file, errno, strerror(errno));
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

int hmd_read_pid_from_file(const char* pid_file, pid_t* pid)
{
    if(!pid_file) {
        P_ERR_STR("Got NULL pid_file");
        return EXIT_FAILURE;
    }
    if(!pid) {
        P_ERR_STR("Got NULL pid");
        return EXIT_FAILURE;
    }
    FILE* fp = fopen(pid_file, "r");
    if(!fp) {
        P_ERR("fopen returned NULL for %s, errno: %d (%s)", pid_file, errno, strerror(errno));
        return EXIT_FAILURE;
    }
    if(fread(pid, sizeof(pid_t), 1, fp) != 1) {
        P_ERR("Failed to read pid to %s, errno: %d (%s)", pid_file, errno, strerror(errno));
        fclose(fp);
        return EXIT_FAILURE;
    }
    if(fclose(fp)) {
        P_ERR("Failed to fclose %s, errno: %d (%s)", pid_file, errno, strerror(errno));
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

