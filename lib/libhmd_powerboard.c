#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdint.h>

#include "libdebug.h"
#include "librpc.h"
#include "libdaemon.h"
#include "powerboard.h"
#include "libhmd_thread.h"
#include "libhmd_powerboard.h"

#define RPC_WAIT (1*1000*1000) // 1 seconds

static hmd_pbd_default_beacon_fs current_beacon;

void* hmd_pbd_thread(void* arg)
{
    P_DEBUG_STR("Started hmd_pbd_thread");

    hmd_thread_arg_fs* args = arg;
    pthread_mutex_lock(&args->thread_lock);

    P_DEBUG_STR("Locked hmd_pbd_thread mutex");

    if(is_daemon_alive(PBD_BIN)) {
        P_ERR_STR("Powerboard was dead, let's restart it");
        startup_daemon(PBD_BIN);
        sleep(5);
    }

    while(rpc_recv_buf(PBD_RPC_SOCKET, RPC_OP_GET_PID, &args->pid, sizeof(args->pid), RPC_WAIT * 5)) {
        P_ERR_STR("Failed to ask powerboard for its PID");
        if(is_daemon_alive(PBD_BIN)) {
            P_ERR_STR("Powerboard was dead, let's restart it");
            args->pid = -1;
            if(startup_daemon(PBD_BIN)) {
                P_ERR_STR("Failed to start powerboard daemon. Keep trying");
            }
        }
        sleep(5);
    }

    P_INFO("Got powerboard PID: %d", (int)args->pid);
    sleep(5);

    while(1) {
        while(is_daemon_alive(PBD_BIN)) {
            P_ERR_STR("Powerboard is dead, let's restart it");
            args->pid = -1;
            if(startup_daemon(PBD_BIN)) {
                P_ERR_STR("Failed to start powerboard daemon. Keep trying");
                sleep(5);
            } else {
                sleep(5);
                if(rpc_recv_buf(PBD_RPC_SOCKET, RPC_OP_GET_PID, &args->pid, sizeof(args->pid), RPC_WAIT * 5)) {
                    P_ERR_STR("Failed to get powerboard pid, continuing anyways");
                }
            }
        }
        sleep(5);
        if(rpc_recv_buf(PBD_RPC_SOCKET, PBD_OP_GET_PACK_VOLT, current_beacon.battery_voltage, sizeof(current_beacon.battery_voltage), RPC_WAIT)) {
            P_ERR_STR("Failed to get voltage from powerboard");
        } else {
            P_DEBUG("Got voltages: %lf, %lf, %lf, %lf", current_beacon.battery_voltage[0], current_beacon.battery_voltage[1], current_beacon.battery_voltage[2], current_beacon.battery_voltage[3]);
        }
        sleep(5);
        if(rpc_recv_buf(PBD_RPC_SOCKET, PBD_OP_GET_PACK_TEMP, current_beacon.battery_temp, sizeof(current_beacon.battery_temp), RPC_WAIT)) {
            P_ERR_STR("Failed to get temperature from powerboard");
        } else {
            P_DEBUG("Got temperatures: %f, %f, %f, %f", current_beacon.battery_temp[0], current_beacon.battery_temp[1], current_beacon.battery_temp[2], current_beacon.battery_temp[3]);
        }
        sleep(5);
    }
    return NULL;
}

int hmd_pbd_thread_get_beacon(hmd_pbd_default_beacon_fs* copy)
{
    if(!copy) {
        P_ERR_STR("Received NULL copy");
        return EXIT_FAILURE;
    }
    memcpy(copy, &current_beacon, sizeof(hmd_pbd_default_beacon_fs));
    return EXIT_SUCCESS;
}

