#ifndef _LIB_LIBHMD_ADCS_H_
#define _LIB_LIBHMD_ADCS_H_

#include <stdint.h>

struct hmd_adcs_default_beacon_fs
{
    double quaternions[4];
    double rates[3];
    uint16_t illumination_state;
    uint8_t algorithm;
};

typedef struct hmd_adcs_default_beacon_fs hmd_adcs_default_beacon_fs;

void* hmd_adcs_thread(void* arg);
int hmd_adcs_thread_get_beacon(hmd_adcs_default_beacon_fs* copy);

#endif
