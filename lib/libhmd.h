#ifndef _LIB_LIBHMD_H_
#define _LIB_LIBHMD_H_

#include <stdint.h>
#include <unistd.h>

#include "libhmd_thread.h"
#include "libhmd_powerboard.h"
#include "libhmd_adcs.h"

#define HMD_RPC_SOCKET UINT16_C(50100)
#define HMD_BIN "hm-d"
#define HMD_LOG_DIR "/var/log/cdh/health_monitor/"
#define HMD_PID_FILE(daemon_) HMD_LOG_DIR#daemon_"-pid"

int fire_thermal_knives(int max_tries, useconds_t wait_us);
int hmd_write_pid_to_file(const char* pid_file, pid_t pid);
int hmd_read_pid_from_file(const char* pid_file, pid_t* pid);

#endif
