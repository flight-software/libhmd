#include <pthread.h>
#include <errno.h>
#include <string.h>

#include "libdebug.h"
#include "libdll.h"
#include "librpc.h"
#include "libadcs.h"
#include "powerboard.h"
#include "libhmd.h"
#include "libhmd_powerboard.h"
#include "libhmd_adcs.h"
#include "libhmd_thread.h"

#define DEFAULT_RPC_WAIT (5000000) // 5 seconds

struct hmd_thread_elem_fs {
    pthread_t thread;
    void* (*thread_main)(void*);
    hmd_thread_arg_fs thread_main_arg;
    int thread_init;
    char thread_name[5];
};

typedef struct hmd_thread_elem_fs hmd_thread_elem_fs;

static dll_fs registered_threads;
static pthread_mutexattr_t mutex_attr;

static hmd_thread_elem_fs* find_thread(const char thread_name[5]);

int hmd_thread_init(void)
{
    if(pthread_mutexattr_init(&mutex_attr)) {
        P_ERR_STR("Failed to initialize attribute mutex, did this function get called twice?");
        return EXIT_FAILURE;
    }
    if(pthread_mutexattr_setrobust(&mutex_attr, PTHREAD_MUTEX_ROBUST)) {
        P_ERR_STR("Failed to set robust attribute of mutex, that's weird");
        pthread_mutexattr_destroy(&mutex_attr);
        return EXIT_FAILURE;
    }
    if(dll_init(&registered_threads, NULL)) {
        P_ERR_STR("Failed to initialize dll");
        pthread_mutexattr_destroy(&mutex_attr);
        return EXIT_FAILURE;
    }
    P_DEBUG_STR("Initialized dll");
    return EXIT_SUCCESS;
}

int hmd_thread_close(void)
{
    DLL_ITER(registered_threads, hmd_thread_elem_fs, curr_elem) {
        hmd_thread_disable(curr_elem->thread_name);
        hmd_thread_deregister(curr_elem->thread_name); // Deallocates
    }
    dll_close(&registered_threads);
    return EXIT_SUCCESS;
}

int hmd_thread_enable_defaults(int powerboard, int adcs)
{
    if(powerboard) {
        if(hmd_thread_register(PBD_BIN, hmd_pbd_thread, NULL)) {
            P_ERR_STR("Failed to register powerboard thread");
            return EXIT_FAILURE;
        }
        P_DEBUG_STR("Registered powerboard thread");
        if(hmd_thread_enable(PBD_BIN)) {
            P_ERR_STR("Failed to enable powerboard thread");
            hmd_thread_deregister(PBD_BIN);
            return EXIT_FAILURE;
        }
    }
    if(adcs) {
        if(hmd_thread_register(ACD_BIN, hmd_adcs_thread, NULL)) {
            P_ERR_STR("Failed to register adcs thread");
            hmd_thread_deregister(PBD_BIN);
            return EXIT_FAILURE;
        }
        P_DEBUG_STR("Registered adcs thread");
        if(hmd_thread_enable(ACD_BIN)) {
            P_ERR_STR("Failed to enable adcs thread");
            hmd_thread_deregister(PBD_BIN);
            hmd_thread_deregister(ACD_BIN);
            return EXIT_FAILURE;
        }
    }
    return EXIT_SUCCESS;
}

int hmd_thread_register(const char thread_name[5], void* (*callback)(void*), void* cb_arg)
{
    if(find_thread(thread_name)) {
        P_ERR("Tried to register duplicate thread name %s", thread_name);
        return EXIT_FAILURE;
    }
    hmd_thread_elem_fs* elem = calloc(1, sizeof(hmd_thread_elem_fs));
    if(!elem) {
        P_ERR("Failed to malloc a new element for %s", thread_name);
        return EXIT_FAILURE;
    }
    elem->thread_main = callback;
    elem->thread_main_arg.cb_arg = cb_arg;
    elem->thread_main_arg.pid = -1;
    memcpy(elem->thread_name, thread_name, 4);
    if(dll_push_tail(&registered_threads, elem, 0, DLL_NOWRITE)) {
        P_ERR("Failed to register %s", thread_name);
        free(elem);
        return EXIT_FAILURE;
    }
    P_DEBUG_STR("Registered a new thread");
    return EXIT_SUCCESS;
}

int hmd_thread_deregister(const char thread_name[5])
{
    hmd_thread_disable(thread_name);
    hmd_thread_elem_fs* found = find_thread(thread_name);
    if(!found) {
        P_ERR("Failed to find registered thread %s", thread_name);
        return EXIT_FAILURE;
    }
    dll_pull_ptr(&registered_threads, found);
    P_DEBUG("Deregistered %s", found->thread_name);
    free(found);
    return EXIT_SUCCESS;
}

int hmd_thread_enable(const char thread_name[5])
{
    hmd_thread_elem_fs* found = find_thread(thread_name);
    if(!found) {
        P_ERR("Failed to find %s thread", thread_name);
        return EXIT_FAILURE;
    }
    P_DEBUG("Successfully found thread %s", found->thread_name);
    if(found->thread_main_arg.pid > 0) {
        P_INFO("Tried to enable %s which is already enabled", found->thread_name);
        return EXIT_SUCCESS;
    }
    if(!found->thread_init) {
        if(pthread_mutex_init(&found->thread_main_arg.thread_lock, &mutex_attr)) {
            P_ERR("Failed to set mutex attributes for %s", found->thread_name);
            return EXIT_FAILURE;
        }
        found->thread_init = 1;
    } else {
        if(!pthread_cancel(found->thread)) { // make sure thread is over
            void* ret;
            pthread_join(found->thread, &ret); // if we actually canceled, lets join
            (void)ret;
        }
    }
    int r;
    if((r = pthread_create(&found->thread, NULL, found->thread_main, &found->thread_main_arg))) {
        P_ERR("Failed to pthread_create for %s, errno: %d (%s)", found->thread_name, r, strerror(r));
        return EXIT_FAILURE;
    }
    P_DEBUG("Created pthread for %s", found->thread_name);
    if((r = pthread_detach(found->thread))) {
        P_ERR("Failed to pthread_detach for %s, errno: %d (%s)", found->thread_name, r, strerror(r));
        pthread_cancel(found->thread);
        return EXIT_FAILURE;
    }
    P_DEBUG("Detached pthread for %s", found->thread_name);
    int errno_out = 0;
    while(hmd_thread_check(found->thread_name, &errno_out) != EXIT_SUCCESS) {
        if(errno_out == EOWNERDEAD) return EXIT_FAILURE;
        sleep(1); // wait for thread to lock
    }
    P_DEBUG("Successfully enabled %s", found->thread_name);
    return EXIT_SUCCESS;
}

int hmd_thread_disable(const char thread_name[5])
{
    hmd_thread_elem_fs* found = find_thread(thread_name);
    if(!found) {
        P_ERR("Failed to find %s", thread_name);
        return EXIT_FAILURE;
    }
    if(found->thread_main_arg.pid == -1) {
        P_INFO("Already disabled %s", found->thread_name);
        return EXIT_SUCCESS;
    }
    P_DEBUG("Found thread for %s", found->thread_name);
    if(pthread_cancel(found->thread)) {
        P_ERR("The pthread of %s was invalid somehow", found->thread_name);
        return EXIT_FAILURE;
    }
    void* ret;
    if(pthread_join(found->thread, &ret)) {
        P_ERR_STR("pthread_join after pthread_cancel should not fail, infinite loop?");
        return EXIT_FAILURE;
    }
    if(ret != PTHREAD_CANCELED) {
        P_ERR_STR("pthread_join did not give us PTHREAD_CANCELED, continuing..");
    }
    {
        int r;
        if((r = pthread_mutex_trylock(&found->thread_main_arg.thread_lock))) {
            if(r != EOWNERDEAD) {
                P_ERR("After canceling %s, we failed to lock the mutex with errno: %d (%s)", found->thread_name, r, strerror(r));
                return EXIT_FAILURE;
            }
            pthread_mutex_consistent(&found->thread_main_arg.thread_lock);
        }
    }
    pthread_mutex_unlock(&found->thread_main_arg.thread_lock);
    pthread_mutex_destroy(&found->thread_main_arg.thread_lock);
    found->thread_main_arg.pid = -1;
    found->thread_init = 0;
    P_DEBUG("Disabled %s", found->thread_name);
    return EXIT_SUCCESS;
}

int hmd_thread_get_pid(const char thread_name[5], pid_t* pid)
{
    hmd_thread_elem_fs* found = find_thread(thread_name);
    if(!found) {
        P_ERR("Failed to find %s", thread_name);
        return EXIT_FAILURE;
    }
    P_INFO("Found %s with pid %d", found->thread_name, found->thread_main_arg.pid);
    *pid = found->thread_main_arg.pid;
    return found->thread_main_arg.pid > 0 ? EXIT_SUCCESS : EXIT_FAILURE;
}

int hmd_thread_check(const char thread_name[5], int* errno_out)
{
    hmd_thread_elem_fs* found = find_thread(thread_name);
    if(!found) {
        P_ERR("Failed to check thread %s", thread_name);
        return EXIT_FAILURE;
    }
    int r;
    if(errno_out) *errno_out = 0;
    if((r = pthread_mutex_trylock(&found->thread_main_arg.thread_lock))) {
        if(errno_out) *errno_out = r;
        if(r == EOWNERDEAD) {
            P_ERR("%s thread died, restarting", found->thread_name);
            if(pthread_mutex_consistent(&found->thread_main_arg.thread_lock)) {
                P_ERR("Failed to mark %s mutex as consistent", found->thread_name);
                return EXIT_FAILURE;
            }
            pthread_mutex_unlock(&found->thread_main_arg.thread_lock);
            P_INFO("%s thread was dead, you should restart it", found->thread_name);
            return EXIT_FAILURE;
        }
        if(r == EBUSY) {
            P_INFO("%s thread had a locked mutex. That's good.", found->thread_name);
            return EXIT_SUCCESS;
        }
        P_ERR("Checking %s thread failed, errno: %d (%s)", found->thread_name, r, strerror(r));
        return EXIT_FAILURE;
    }
    P_ERR("Managed to lock the %s lock, you might want to restart it", found->thread_name);
    pthread_mutex_unlock(&found->thread_main_arg.thread_lock);
    return EXIT_FAILURE;
}

int hmd_thread_check_all(void)
{
    int r = EXIT_SUCCESS;
    DLL_ITER(registered_threads, hmd_thread_elem_fs, curr_elem) {
        hmd_thread_elem_fs* f = curr_elem;
        if(hmd_thread_check(f->thread_name, NULL)) {
            r = EXIT_FAILURE;
        }
    }
    return r;
}

static hmd_thread_elem_fs* find_thread(const char thread_name[5])
{
    DLL_ITER(registered_threads, hmd_thread_elem_fs, curr_elem) {
        if(memcmp(curr_elem->thread_name, thread_name, 4) == 0) {
            return curr_elem;
        }
    }
    return NULL;
}

